# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 14:28:02 2019

@author: Romain B
"""
import matplotlib.pyplot as plt
import numpy as np

def plot_points(filename):
    x=[]
    y=[]
    A=0
    G2x=0
    G2y=0
    points=np.loadtxt(open(filename,"r"),delimiter=" ")
    
    # Insertion des points dans des listes
    for row in points:
        x.append(int(row[0]))
        y.append(int(row[1]))
        
    n=len(points)
    
    # Calcul du centre de gravite geographique et de l'aire
    for i in range(n-1):
        A+=x[i]*y[i+1]-x[i+1]*y[i]
        G2x+=(x[i]+x[i+1])*(x[i]*y[i+1]-x[i+1]*y[i])
        G2y+=(y[i]+y[i+1])*(x[i]*y[i+1]-x[i+1]*y[i])

    # Calcul et formattage du centre gravite statistique
    G=(np.mean(x),np.mean(y))
    
    # Formattage du centre gravite geographique
    G2=(G2x/(6*A/2), G2y/(6*A/2))
    plt.figure(1)
    plt.plot(x,y,G[0],G[1],"g+",G2[0],G2[1],"ro")
    plt.show()
    print("Centre de gravité statistique: (",G[0],",",G[1],")")
    print("Centre de gravité géographique: (",G2[0],",",G2[1],")")
    
    a=b=c=0
    for i in range(n-1):
        a+=x[i]*x[i]
        b+=x[i]*y[i]
        c+=y[i]*y[i]
    b*=2
    
    # Vu que di/dalpha = 0
    # avec alpha compris entre -pi/2 et pi/2
    alpha=0.5*np.arctan(b/(a-c))
    
    I=( 0.5*(a+c) ) - ( 0.5*(a-c)*np.cos(2*alpha) ) - ( 0.5*b*np.sin(2*alpha) )
    print("I = ",I) 
    
    #Angle de la pente de l'inertie
    teta=alpha
    if( (( 2*(a-c)*np.cos(2*alpha) ) + ( 2*b*np.sin(2*alpha) )) < 0):
        teta+=(np.pi/2)
    
    print("Angle de la pente : ", teta*180/np.pi, "degrés")
    
    plt.figure(1)
    plt.plot(x,y,G[0],G[1],"g+",G2[0],G2[1],"ro")
    plt.quiver(*G2,I*np.cos(teta),I*np.sin(teta))
    plt.show()
    
    # Average Bending energy
    #BE=0
    #for i in range(n): K(s)² nani ?
        
    # Eccentricity
    ## Méthode des axes principals
    
    # Cxy=Cyx
    Cxx=Cxy=Cyy=0
    for i in range(n):
        Cxx+=np.power(x[i]-G2[0],2)
        Cxy+=(x[i]-G2[0])*(y[i]-G2[1])
        Cyy+=np.power(y[i]-G2[1],2)
    Cxx/=n
    Cxy/=n
    Cyy/=n
    
    sqrt_part=np.sqrt( np.power(Cxx+Cyy,2) - 4*( (Cxx*Cyy) - np.power(Cxy ,2) ) )
    l1=0.5*(Cxx+Cyy+sqrt_part)
    l2=0.5*(Cxx+Cyy-sqrt_part)
    
    E = l2/l1
    
    print("Eccentricité par axes principals : ",E)
    
    ## Méthode du minimum bounding rectangle
    ### Surement faux car tres probablement pas minimal
    W=max(x)-min(x)
    H=max(y)-min(y)
    E2=1-(W/H if W<H else H/W)
    
    print("Eccentricité par minimum bounding rectangle : ",E2)
    
    
    # Circularity Ratio
    
    ## Circle : ratio area/perimeter²
    figure = Polygon(*points)
    # As : aire de la forme
    As = np.abs(float(figure.area))
    # Ac : aire du cercle ayant le même périmètre que la forme
    radius = As / 2 * np.pi
    Ac = np.pi * radius ** 2  # area of circle
    
    C=As/Ac
    print("Circularity ratio (As/Ac) : ",C)
    

    ## Circle variance
    di=mu_r=sigma_r=0
    for i in range(1,n):
        di+=np.sqrt( np.power(x[i]-G2[0],2) + np.power(y[i]-G2[1],2) )
        mu_r+=di
        sigma_r+=np.power(di-mu_r,2)
    mu_r/=n
    sigma_r=np.sqrt(sigma_r/n)
    
    Cva=sigma_r/mu_r
    
    print("Déviation moyenne du centre : ",mu_r)
    print("Déviation standard du centre : ",sigma_r)
    print("Circularity ratio : ", Cva)
    
    

plot_points("1.txt")