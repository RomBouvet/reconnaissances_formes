# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 18:24:03 2019

@author: Romain B
"""

import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from math import log2

def points_from_txt(filename):
    return np.loadtxt(open(filename,"r"),delimiter=" ")

def coord_from_points(points):
    x=[]
    y=[]
    for row in points:
        x.append(int(row[0]))
        y.append(int(row[1]))
    return x,y

def init_degrees(n):
    ki=[]
    for i in range(n):
        ki.append(n-1)
    return ki

def create_weight_matrix(x,y):
    W=[]
    for i in range(len(x)):
        for j in range(len(y)):
            W.append(sqrt( ((x[i]-x[j])**2) + ((y[i]-y[j])**2) ))
    W = np.array(W)
    W = W.reshape(len(x),len(y))
    W = W/np.max(W)
    return W

def get_unweighted_matrix(W,T):
    A=[]
    for i in range(W.shape[0]):
        for j in range(W.shape[1]):
            A.append( (1 if W[i][j]<T else 0) )
    A = np.array(A)
    A = A.reshape(W.shape[0],W.shape[1])
    A = A/np.max(A)
    return A

def get_degrees(A):
    k=[]
    for i in range(A.shape[0]):
        k.append(np.count_nonzero(A[i])-1)
    return k

# def clustering-coef ? 
    
def get_dynamic_evolution(W,start,end,steps):
    inc=(end-start)/steps
    ret=[]
    for i in range(steps):
        ret.append(get_unweighted_matrix(W,start+(i*inc)))
    return ret

def get_dynamic_degrees(dynamic_evolution):
    ret=[]
    for e in dynamic_evolution:
        ret.append(get_degrees(e))
    return ret

def get_normalized_degrees(ki):
    ret=[]
    for step in ki:
        ret.append([x / len(step) for x in step])
    return ret

def get_kK(ki):
    ret=[]
    for e in ki:
        ret.append(max(e))
    return ret

def get_kmu(ki):
    ret=[]
    for e in ki:
        ret.append(np.mean(e))
    return ret

def get_degree_descriptor(kmu,kK):
    ret=[]
    for i in range(len(kmu)):
        ret.append(kmu[i])
        ret.append(kK[i])
    return ret

def get_pi(A):
    Pi=[]
    k=get_degrees(A)
    for i in range(A.shape[0]):
        M=0
        if(k[i]!=0):
            for j in range(A.shape[1]):
                if i==j:
                    pass
                if A[i][j]==1 and k[i]==k[j]:
                    M+=1
            Pi.append(M/k[i])
        else:
            Pi.append(0)
    return Pi

def get_dynamic_pi(dynamic_evolution):
    ret=[]
    for i in range(len(dynamic_evolution)):
        ret.append(get_pi(dynamic_evolution[i]))
    return ret

def get_entropy(pi):
    ret=0
    try:  
        for i in range(len(pi)):
            ret+=pi[i]*log2(pi[i])
        return -ret
    except ValueError:
        print("-inf detected. return -99999")
        return -99999

def get_energy(pi):
    return sum(pi)**2

def get_avg_joint_degree(pi):
    return np.mean(pi)

def get_joint_degree_descriptor(dynamic_evolution):
    pi=get_dynamic_pi(dynamic_evolution)
    ret=[]
    for i in range(len(pi)):
        ret.append(get_entropy(pi[i]))
        ret.append(get_energy(pi[i]))
        ret.append(get_avg_joint_degree(pi[i]))
    return ret

def main(filename):
    #threshold=0.0001
    start=0.01
    end=0.50
    
    steps=3
    x,y=coord_from_points(points_from_txt(filename))
    plt.plot(x,y)
    W=create_weight_matrix(x,y)
    print(W)
    #A=get_unweighted_matrix(W,threshold)
    #print(A)
    #k=get_degrees(A)
    #print(k)
    #get_clustering_coefficient(k)
    dynamic_evolution=get_dynamic_evolution(W,start,end,steps)
    ki=get_dynamic_degrees(dynamic_evolution)
    #print(ki)
    ki=get_normalized_degrees(ki)
    #print(ki)
    kK=get_kK(ki)
    kmu=get_kmu(ki)
    print(get_degree_descriptor(kmu,kK))
    print(get_joint_degree_descriptor(dynamic_evolution))
    
    
main("tests/1.txt")